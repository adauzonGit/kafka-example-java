package com.adauzon.kafka.producers;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;
import java.util.concurrent.ExecutionException;

public class Producers {


    private static final Logger log = LoggerFactory.getLogger(Producers.class);

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:9092");
        props.put("acks", "1");
        props.put("key.serializer",
                "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer",
                "org.apache.kafka.common.serialization.StringSerializer");
        props.put("linger.ms","10");
        try (Producer<String, String> producer = new
                KafkaProducer<>(props);) {
            for (int i = 0; i < 100; i++) {
                producer.send(new
                        ProducerRecord<>("example-topic-v2", "key : ", i + ": message"));//.get();
            }
            producer.flush();
        }
        // 6428
        // 6361
        // 6194
        log.info("End process {}", System.currentTimeMillis() - start);

    }

}
