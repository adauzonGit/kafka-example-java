package com.adauzon.kafka.multithread;

import com.adauzon.kafka.producers.Producers;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.WakeupException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicBoolean;

public class ThreadConsumer extends Thread{


    private static final Logger log = LoggerFactory.getLogger(ThreadConsumer.class);
    private final KafkaConsumer<String,String> consumer;

    private AtomicBoolean close = new AtomicBoolean(false);

    public ThreadConsumer(KafkaConsumer<String, String> consumer) {
        this.consumer = consumer;
    }

    @Override
    public void run() {

        consumer.subscribe(Collections.singleton("example-topic-v2"));


            try {
                while (!close.get()) {
                    ConsumerRecords<String, String> records =
                            consumer.poll(Duration.ofMillis(100));
                    for (ConsumerRecord<String, String> record
                            : records)
                        log.info("offset = {}, partition {} key = {}, value ={}",
                                record.offset(), record.partition() , record.key(), record.value());
                }
            }catch (WakeupException wakeupException){
                if(!close.get()){
                    throw wakeupException;
                }
            }finally {
                consumer.close();

            }


    }

    public void shutdown(){
        close.set(true);
        consumer.wakeup();
    }
}
