package com.adauzon.kafka.consumer;

import com.adauzon.kafka.producers.Producers;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

public class Consumer {

    private static final Logger log = LoggerFactory.getLogger(Producers.class);

    public static void main(String[] args) {
        Properties props = new Properties();
        props.setProperty("bootstrap.servers", "localhost:9092");
        props.setProperty("group.id", "local-group");
        props.setProperty("enable.auto.commit", "true");
        props.setProperty("auto.commit.interval.ms", "1000");
        props.setProperty("key.deserializer",
                "org.apache.kafka.common.serialization.StringDeserializer");
        props.setProperty("value.deserializer",
                "org.apache.kafka.common.serialization.StringDeserializer");
        try (KafkaConsumer<String, String> consumer = new
                KafkaConsumer<>(props);) {
//            consumer.subscribe(List.of("example-topic-v2"));
            TopicPartition topicPartition = new TopicPartition("example-topic-v2",1);
            consumer.assign(Collections.singleton(topicPartition));
            consumer.seek(topicPartition,320800);
            while (true) {
                ConsumerRecords<String, String> records =
                        consumer.poll(Duration.ofMillis(100));
                for (ConsumerRecord<String, String> record
                        : records)
                    log.info("offset = {}, key = {}, partition {}, value ={}",
                            record.offset(),  record.key(), record.partition(), record.value());
            }
        }

    }
}
