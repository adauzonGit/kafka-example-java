package com.adauzon.kafka.transactional;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.Properties;

import static org.apache.kafka.common.requests.DeleteAclsResponse.log;

public class TransactionalProducer {


    public static void main(String[] args) {
        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:9092");
        props.put("acks", "all");
        props.put("compression.type", "gzip");
        props.put("linger.ms", "1");
        props.put("batch.size", "32384");
        props.put("transactional.id", "local-producer");
        props.put("buffer.memory", "33554432");
        props.put("key.serializer",
                "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer",
                "org.apache.kafka.common.serialization.StringSerializer");
        try (Producer<String, String> producer = new
                KafkaProducer<>(props)) {
            try {
                producer.initTransactions();
                producer.beginTransaction();
                for (int i = 0; i < 1_000_000; i++) {
                    producer.send(new
                            ProducerRecord<>("example-topic-v2", "message"));
                    if (i == 100) {
//                        throw new Exception("Random exception");
                    }
                }
                producer.commitTransaction();
            } catch (Exception e) {
                producer.abortTransaction();
                log.error("Error processing messages", e);
            }
        }

    }
}
