package com.adauzon.kafka.transactional;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;

import static org.apache.kafka.common.requests.DeleteAclsResponse.log;

public class TransactionalConsumer {

    public static void main(String[] args) {
        Properties props = new Properties();
        props.setProperty("bootstrap.servers", "localhost:9092");
        props.setProperty("group.id", "local-group");
        props.setProperty("enable.auto.commit", "true");
        props.setProperty("auto.commit.interval.ms", "1000");
        props.setProperty("key.deserializer",
                "org.apache.kafka.common.serialization.StringDeserializer");
        props.setProperty("value.deserializer",
                "org.apache.kafka.common.serialization.StringDeserializer");
        props.setProperty("isolation.level", "read_committed");
        try (KafkaConsumer<String, String> consumer = new
                KafkaConsumer<>(props);) {
            consumer.subscribe(Arrays.asList("example-topic-v2"));
            while (true) {
                ConsumerRecords<String, String> records =
                        consumer.poll(Duration.ofMillis(100));
                for (ConsumerRecord<String,
                        String> record : records)
                    log.info("partition = {} , offset = {}, key = {},  value = {}", record.partition(), record.offset(),
                            record.key(), record.value());
            }
        }
    }
}
