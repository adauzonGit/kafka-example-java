package com.adauzon.kafka.callbacks;

import com.adauzon.kafka.producers.Producers;
import org.apache.kafka.clients.producer.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

public class CallBackProducer {

    private static final Logger log = LoggerFactory.getLogger(CallBackProducer.class);

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:9092");
        props.put("acks", "all");
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("linger.ms", "10");
        try (Producer<String, String> producer = new KafkaProducer<>(props);) {
            for (int i = 0; i < 10; i++) {
                producer.send(new ProducerRecord<>("example-topic-v2", "message"), (metadata, exception) -> {
                    if (exception != null) {
                        log.error("Error sending the message", exception);
                    }
                    log.info("Partition = {} Offset ={}", metadata.partition(), metadata.offset());
                });
            }
        }

    }
}
