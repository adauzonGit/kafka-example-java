package com.adauzon.springkafka;

import io.micrometer.core.instrument.MeterRegistry;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.config.KafkaListenerEndpointRegistry;
import org.springframework.kafka.core.KafkaSendCallback;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.listener.MessageListenerContainer;
import org.springframework.kafka.support.SendResult;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import java.util.List;
import java.util.Objects;

@SpringBootApplication
@Slf4j
@EnableScheduling
public class SpringKafkaApplication
//        implements CommandLineRunner
{

    public static void main(String[] args) {
        SpringApplication.run(SpringKafkaApplication.class, args);
    }

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;
//    public Role createRole(Role role) {
//        Role save = repository.save(role);
//        kafkaTemplate.send("devs4j-topic",save.getName());
//        return save;
//    }

    @Autowired
    private MeterRegistry meterRegistry;

    @Autowired
    private KafkaListenerEndpointRegistry kafkaListenerEndpointRegistry;

    @KafkaListener(id = "example-id", autoStartup = "true", topics = "example-topic-v2", containerFactory = "kafkaListenerContainerFactory", groupId = "consumer", properties = {"max.poll.interval.ms:60000", "max.poll.records:50"})
    public void listen(List<ConsumerRecord<String, String>> message) {
        log.info("Batch size ");
        for (ConsumerRecord<String, String> s : message) {

//            log.info("Partion {}, value {} , offset {}, key {} ", s.partition(), s.value(), s.offset(), s.key());
        }
        log.info("End batcdh");
    }

    @Scheduled(fixedDelay = 2000, initialDelay = 500)
    public void schedule() {
        log.info("Hi!");
        for (int i = 0; i < 100; i++) {
            kafkaTemplate.send("example-topic-v2", String.valueOf(i), "put play : " + i);
        }

    }

    @Scheduled(fixedDelay = 2000, initialDelay = 500)
    public void printMetrics() {
        double count = meterRegistry.get("kafka.producer.record.send.total").functionCounter().count();
        log.info("Count {} ", count);

    }

    //    @Override
    public void run(String... args) throws Exception {

//        for (int i = 0; i < 100; i++) {
//            kafkaTemplate.send("example-topic-v2", String.valueOf(i), "put play : " + i);
//        }
//        log.info("waiting to start");
//        Thread.sleep(5000);
//        log.info("start the consumer");
//        MessageListenerContainer listenerContainer = kafkaListenerEndpointRegistry.getListenerContainer("example-id");
//        Objects.requireNonNull(listenerContainer).start();
//        Thread.sleep(5000);
//        listenerContainer.stop();
//        ListenableFuture<SendResult<Integer, String>> f = kafkaTemplate.send("example-topic-v2", "put play");
//        f.addCallback(new ListenableFutureCallback<SendResult<Integer, String>>() {
//            @Override
//            public void onFailure(Throwable ex) {
//                log.error("Meesage error ",ex);
//            }
//
//            @Override
//            public void onSuccess(SendResult<Integer, String> result) {
//
//                log.info("Send {}", result.getRecordMetadata().offset());
//            }
//        });
    }
}
